import { createMongooseSchema } from 'convert-json-schema-to-mongoose';
import Table from 'easy-table';

console.log('convert');

const sch = {
  title: 'Title of schema',
  type: 'object',
  properties: {
    category: {
      type: 'string',
      __lookup__: 'category-list',
    },
    freeText: {
      type: 'string',
    },
    count: {
      type: 'integer',
      min: 200,
      default: 300
    },
    type: {
      type: 'string',
    },
  }
};
const schema =
  {
    type: 'object',
    properties:
      {
        id:
          {
            type: 'string'
          },
        address:
          {
            type: 'object',
            properties:
              {
                street: {type: 'string', default: '44', pattern: '^\\d{2}$'},
                houseColor: {type: 'string', default: '[Function=Date.now]', format: 'date-time'}
              }
          }
      }
  };

const mongooseSchema = createMongooseSchema({}, sch);

console.log(JSON.stringify(mongooseSchema, null, 2));

const t = new Table();

const jSchemaLines = JSON.stringify(sch, null, 2).split('\n');
const mSchemaLines = JSON.stringify(mongooseSchema, null, 2).split('\n');

for (let i=0; i<Math.max(jSchemaLines.length, mSchemaLines.length); i++) {
  t.cell('json-schema', (i<jSchemaLines.length) ? jSchemaLines[i] : '');
  t.cell('    ', '   |   ');
  t.cell('mongoose-schema', (i<mSchemaLines.length) ? mSchemaLines[i] : '');
  t.newRow();
}
console.log(t.toString());
