import Ajv, {_, nil, KeywordCxt, Code} from 'ajv';

import { createMongooseSchema } from 'convert-json-schema-to-mongoose';
import Table from 'easy-table';
import {DataValidationCxt} from 'ajv/dist/types';
import {makeLookupManager} from './lookup-manager';

console.log('addKeyword');

const ajv = new Ajv();

ajv.addKeyword({
  keyword: 'range',
  type: 'number',
  code(cxt: KeywordCxt) {
    const {schema, parentSchema, data} = cxt;
    const [min, max] = schema;
    const eq: Code = parentSchema.exclusiveRange ? _`=` : nil;
    cxt.fail(_`${data} <${eq} ${min} || ${data} >${eq} ${max}`);
  },
  metaSchema: {
    type: 'array',
    items: [{type: 'number'}, {type: 'number'}],
    minItems: 2,
    additionalItems: false,
  },
});

const lookupMgr = makeLookupManager();

ajv.addKeyword({
  keyword: '__lookup__',
  type: 'string',
  validate: (keyword: string, data: any, dataCxt?: DataValidationCxt): boolean => {
    return lookupMgr.exists(keyword, data);
  },
  // code(cxt: KeywordCxt) {
  //   const {schema, parentSchema, data} = cxt;
  //   const lu = schema;
  //   cxt.$data;
  //   cxt.fail(_`${data} != ${lu}`);
  // },
  metaSchema: {
    type: 'string',
    items: [{type: 'number'}, {type: 'number'}],
    minItems: 2,
    additionalItems: false,
  },
});

lookupMgr.updateLookup('category-list', ['a', 'b', 'c']);

const sch = {
  title: 'Title of schema',
  type: 'object',
  properties: {
    category: {
      type: 'string',
      __lookup__: 'category-list',
    },
    freeText: {
      type: 'string',
    },
    catArray: {
      type: 'array',
      items: {
        type: 'string',
        __lookup__: 'category-list',
      }
    },
    count: {
      type: 'integer',
      range: [50, 300],
      default: 300
    },
    type: {
      type: 'string',
    },
  }
};

const mongooseSchema = createMongooseSchema({}, sch);

// console.log(JSON.stringify(mongooseSchema, null, 2));

// const t = new Table();
// const jSchemaLines = JSON.stringify(sch, null, 2).split('\n');
// const mSchemaLines = JSON.stringify(mongooseSchema, null, 2).split('\n');
//
// for (let i=0; i<Math.max(jSchemaLines.length, mSchemaLines.length); i++) {
//   t.cell('json-schema', (i<jSchemaLines.length) ? jSchemaLines[i] : '');
//   t.cell('    ', '   |   ');
//   t.cell('mongoose-schema', (i<mSchemaLines.length) ? mSchemaLines[i] : '');
//   t.newRow();
// }
// console.log(t.toString());

const isValid = ajv.validate(sch, {
  category: 'a',
  catArray: [
    'c',
    'b',
  ],
  count: 200,
});

console.log(isValid);
console.log(JSON.stringify(ajv.errors, null, 2));
