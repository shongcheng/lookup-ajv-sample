import Ajv from 'ajv';
import {LookupManager} from './lookup-manager';
import {DataValidationCxt} from 'ajv/dist/types';

/**
 * Adds the custom "`__lookup__`" keyword to the specified `Ajv` instance
 * @param ajv
 * @param lookupManager
 * @returns The Ajv instance given in params to enable fluent API
 */
export const addLookupToAjv = (ajv: Ajv, lookupManager: LookupManager) => {
  ajv.addKeyword({
    keyword: '__lookup__',
    type: 'string',
    validate: (keyword: string, data: any, dataCxt?: DataValidationCxt): boolean => {
      return lookupManager.exists(keyword, data);
    },
    metaSchema: {
      type: 'string',
      items: [{type: 'number'}, {type: 'number'}],
      minItems: 2,
      additionalItems: false,
    },
  });
  return ajv;
};
