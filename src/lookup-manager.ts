import {lookup} from 'dns';

export class LookupManager {
  private pLookupMap: Map<string, Set<string>> = new Map();

  getLookup(lookupName: string): Set<string> | undefined {
    return this.pLookupMap.get(lookupName);
  }

  /**
   * May be called anytime to refresh the specified `lookup`,
   * if specified `lookup` does not exist,
   *    it will be created with the specified `values`.
   * For e.g, an app may periodically reload valid values from
   * database and update using this method anytime.
   *
   * @param lookupName
   * @param values
   */
  updateLookup(lookupName: string, values: string[]) {
    const newSet = new Set<string>();
    values.forEach(v => {
      newSet.add(v);
    });
    this.pLookupMap.set(lookupName, newSet);
  }

  /**
   * Checks if the given value exists in the given lookup.
   *
   * @param lookup Unique name of the lookup to check against
   * @param value
   */
  exists(lookupName: string, value: string): boolean {
    const lookup = this.getLookup(lookupName);
    if (lookup) {
      return lookup.has(value);
    } else {
      return false;
    }
  }

}

/**
 * Factory function to create a new LookupManager instance
 */
export const makeLookupManager = () => {
  return new LookupManager();
};
