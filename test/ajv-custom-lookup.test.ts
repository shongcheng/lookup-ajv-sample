import {LookupManager, makeLookupManager} from '../src/lookup-manager';
import Ajv from 'ajv';
import {addLookupToAjv} from '../src/lookup-ajv';

describe('ajv-custom-lookup', () => {

  let mgr: LookupManager = undefined as unknown as LookupManager;
  let customAjv = undefined as unknown as Ajv;

  beforeEach(() => {
    mgr = makeLookupManager();
    customAjv = addLookupToAjv(new Ajv(), mgr);
  });

  it('customLookupAjv validates the lookup', async () => {
    mgr.updateLookup('valid-categories', ['cat1', 'cat2', 'cat3']);
    mgr.updateLookup('valid-types', ['type2', 'type1', 'type3']);
    const schema = {
      title: 'Title of schema',
      type: 'object',
      properties: {
        category: {
          type: 'string',
          __lookup__: 'valid-categories',
        },
        freeText: {
          type: 'string',
        },
        typeArray: {
          type: 'array',
          items: {
            type: 'string',
            __lookup__: 'valid-types',
          }
        },
        count: {
          type: 'integer',
        },
        type: {
          type: 'string',
        },
      }
    };
    const validate = customAjv.compile(schema);
    expect(validate({
      category: 'cat1',
      typeArray: [
        'type1',
        'type2',
      ],
      count: 200,
    })).toBe(true);
  });

  it('customLookupAjv allows schemas with no __lookup__ keywords', async () => {
    mgr.updateLookup('valid-categories', ['cat1', 'cat2', 'cat3']);
    mgr.updateLookup('valid-types', ['type2', 'type1', 'type3']);
    const schema = {
      title: 'Title of schema',
      type: 'object',
      properties: {
        category: {
          type: 'string',
        },
        freeText: {
          type: 'string',
        },
        typeArray: {
          type: 'array',
          items: {
            type: 'string',
          }
        },
        count: {
          type: 'integer',
        },
        type: {
          type: 'string',
        },
      }
    };
    const validate = customAjv.compile(schema);
    expect(validate({
      category: 'cat1',
      typeArray: [
        'type1',
        'type2',
      ],
      count: 200,
    })).toBe(true);
  });

  it('customLookupAjv detects invalid values in string property not matching any __lookup__', async () => {
    mgr.updateLookup('valid-categories', ['cat1', 'cat2', 'cat3']);
    mgr.updateLookup('valid-types', ['type2', 'type1', 'type3']);
    const schema = {
      title: 'Title of schema',
      type: 'object',
      properties: {
        category: {
          type: 'string',
          __lookup__: 'valid-categories',
        },
        freeText: {
          type: 'string',
        },
        typeArray: {
          type: 'array',
          items: {
            type: 'string',
            __lookup__: 'valid-types',
          }
        },
        count: {
          type: 'integer',
        },
        type: {
          type: 'string',
        },
      }
    };
    const validate = customAjv.compile(schema);
    const isValid = validate({
      category: 'invalid cat1',
      typeArray: [
        'type1',
        'type2',
      ],
      count: 200,
    });

    expect(validate.errors?.length).toEqual(1);
    expect(validate.errors?.[0].schemaPath).toEqual('#/properties/category/__lookup__');
    expect(isValid).toBe(false);
  });

  it('customLookupAjv detects invalid values in element of array property not matching any __lookup__', async () => {
    mgr.updateLookup('valid-categories', ['cat1', 'cat2', 'cat3']);
    mgr.updateLookup('valid-types', ['type2', 'type1', 'type3']);
    const schema = {
      title: 'Title of schema',
      type: 'object',
      properties: {
        category: {
          type: 'string',
          __lookup__: 'valid-categories',
        },
        freeText: {
          type: 'string',
        },
        typeArray: {
          type: 'array',
          items: {
            type: 'string',
            __lookup__: 'valid-types',
          }
        },
        count: {
          type: 'integer',
        },
        type: {
          type: 'string',
        },
      }
    };
    const validate = customAjv.compile(schema);
    const isValid = validate({
      category: 'cat1',
      typeArray: [
        'type1',
        'invalid type',
        'type2',
      ],
      count: 200,
    });

    expect(validate.errors?.length).toEqual(1);
    expect(validate.errors?.[0].schemaPath).toEqual('#/properties/typeArray/items/__lookup__');
    expect(isValid).toBe(false);
  });
});
