import {LookupManager, makeLookupManager} from '../src/lookup-manager';

describe('lookup-manager', () => {

  let mgr: LookupManager = undefined as unknown as LookupManager;

  beforeEach(() => {
    mgr = makeLookupManager();
  });

  it('getLookup() Returns undefined if requested lookup has not been added before', async () => {
    expect(mgr.getLookup('non-existent-lookup')).toBe(undefined);
  });

  it('getLookup() Returns a valid Set if requested lookup has been added before', async () => {
    mgr.updateLookup('list-1', ['a', 'b']);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(mgr.getLookup('list-1').size).toEqual(2);
  });

  it('exists() Returns true if specified value exists', async () => {
    mgr.updateLookup('list-aaa-bbb-ccc', ['aaa', 'bbb', 'ccc']);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(mgr.getLookup('list-aaa-bbb-ccc').size).toEqual(3);
    expect(mgr.exists('list-aaa-bbb-ccc', 'bbb')).toBe(true);
  });
});
