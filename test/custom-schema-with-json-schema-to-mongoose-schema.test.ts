import {LookupManager, makeLookupManager} from '../src/lookup-manager';
import Ajv from 'ajv';
import {addLookupToAjv} from '../src/lookup-ajv';
import {createMongooseSchema} from 'convert-json-schema-to-mongoose';

describe('convert-json-schema-to-mongoose', () => {

  it(' safely ignores additional custom JSON schema keywords __lookup__ ', async () => {
    const schema = {
      title: 'Title of schema',
      type: 'object',
      properties: {
        category: {
          type: 'string',
          __lookup__: 'valid-categories',
        },
        freeText: {
          type: 'string',
        },
        typeArray: {
          type: 'array',
          items: {
            type: 'string',
            __lookup__: 'valid-types',
          }
        },
        count: {
          type: 'integer',
        },
        type: {
          type: 'string',
        },
      }
    };

    const mongooseSchema = createMongooseSchema({}, schema);

    expect(JSON.stringify(mongooseSchema.category)).toEqual('{}');
    expect(JSON.stringify(mongooseSchema.freeText)).toEqual('{}');
    expect(JSON.stringify(mongooseSchema.typeArray)).toEqual('[{}]');
    expect(JSON.stringify(mongooseSchema.count)).toEqual('{}');
    expect(JSON.stringify(mongooseSchema.type)).toEqual('{}');

  });
});
