# lookup-ajv-sample

## About

Sample implementation of adding custom JSON Schema keyword to Ajv for custom validation
lookup values that can be provided and changed during run-time.

This project is created as a Feathers app mainly for the easy setup of `JEST`.

Command to run the tests: `npm run test`

## `__lookup__` keyword

The custom keyword is `__lookup__`, starts and ends with double-underscores so that the custom
keyword can be easily identified if there is need to automatically remove the custom
keyword fron the schema for further processing.

Sample usage can be found at [test/ajv-custom-lookup.test.ts](test/ajv-custom-lookup.test.ts)

## Key Files

The following files are the key samples, demonstrating the use of custom keyword `__lookup__`
to specify the name of the lookup list to use.

- `src/lookup-ajv.ts`
- `src/lookup-manager.ts`
- `test/ajv-custom-lookup.test.ts`
- `test/lookup-manager.test.ts`
- `test/custom-schema-with-json-schema-to-mongoose-schema.test.ts`
  - simple test to verify the additional custom keyword does not crash `convert-json-schema-to-mongoose`
  
